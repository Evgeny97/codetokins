package old;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.comments.LineComment;
import com.github.javaparser.ast.nodeTypes.modifiers.NodeWithAbstractModifier;
import com.github.javaparser.printer.YamlPrinter;

import static com.github.javaparser.utils.Utils.assertNotNull;
import static java.util.stream.Collectors.toList;

import java.util.List;

import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.metamodel.NodeMetaModel;
import com.github.javaparser.metamodel.PropertyMetaModel;

public class  MyPrinter extends YamlPrinter {
    public MyPrinter(boolean outputNodeType) {
        super(outputNodeType);
    }

    private String indent(int level) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < level; ++i) {
            for (int j = 0; j < 4; ++j) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }

    private String escapeValue(String value) {
        return "\"" + value.replace("\\", "\\\\").replaceAll("\"", "\\\\\"").replace("\n", "\\n").replace("\t", "\\t") + "\"";
    }

    @Override
    public void output(Node node, String name, int level, StringBuilder builder) {
        assertNotNull(node);

        NodeMetaModel metaModel = node.getMetaModel();
        if (node instanceof Comment) {
            return;
        }
        List<PropertyMetaModel> allPropertyMetaModels = metaModel.getAllPropertyMetaModels();
        List<PropertyMetaModel> attributes = allPropertyMetaModels.stream().filter(PropertyMetaModel::isAttribute)
                .filter(PropertyMetaModel::isSingular).collect(toList());
        List<PropertyMetaModel> subNodes = allPropertyMetaModels.stream().filter(PropertyMetaModel::isNode)
                .filter(PropertyMetaModel::isSingular).collect(toList());
        List<PropertyMetaModel> subLists = allPropertyMetaModels.stream().filter(PropertyMetaModel::isNodeList)
                .collect(toList());

        System.out.print(System.lineSeparator() + indent(level) + name + "(Type=" + metaModel.getTypeName() + "): ");
        if (node.getComment().isPresent()) {
            System.out.print(System.lineSeparator() + indent(level) + "// " + node.getComment().get().toString());
        }
        if(node instanceof NodeWithAbstractModifier){
            NodeWithAbstractModifier node2 = (NodeWithAbstractModifier)node;
            System.out.print(System.lineSeparator()+node2.isAbstract());
        }

        level++;

        for (PropertyMetaModel a : attributes) {
            System.out.print(System.lineSeparator() + indent(level) + a.getName() + ": " + escapeValue(a.getValue(node).toString()));
        }

        for (PropertyMetaModel sn : subNodes) {

            Node nd = (Node) sn.getValue(node);
            if (nd != null)
                output(nd, sn.getName(), level, builder);
        }

        for (PropertyMetaModel sl : subLists) {
            NodeList<? extends Node> nl = (NodeList<? extends Node>) sl.getValue(node);

            if (nl != null && nl.isNonEmpty()) {
                System.out.print(System.lineSeparator() + indent(level) + sl.getName() + ": ");
                String slName = sl.getName();
                slName = slName.endsWith("s") ? slName.substring(0, sl.getName().length() - 1) : slName;
                for (Node nd : nl)
                    output(nd, "- " + slName, level + 1, builder);
            }
        }
    }
}
