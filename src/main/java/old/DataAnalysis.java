package old;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.comments.Comment;
import com.github.javaparser.ast.nodeTypes.modifiers.NodeWithAbstractModifier;
import com.github.javaparser.metamodel.NodeMetaModel;
import com.github.javaparser.metamodel.PropertyMetaModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

import static com.github.javaparser.utils.Utils.assertNotNull;
import static java.util.stream.Collectors.toList;

public class DataAnalysis {

  /*

    public int CountCommentedMethods() {
        List<String> parsedFiles = getAllFiles(new File("parsedFiles"));
        int counter = 0;
        for (String filePath : parsedFiles) {
            if(counter>100){
                return counter;
            }
            System.out.println("Parsing file: " + filePath + "; counter = " + counter);
            try {
            counter += CountComMethodsFromFile(filePath);
            }catch (Exception e){
                System.err.println(e.getMessage());;
            }
        }

        return counter;
    }

    private int CountComMethodsFromFile(String filePath) {
        try {
            CompilationUnit cu = JavaParser.parse(new File(filePath));
            return output(cu, 0, 0);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        }
        return 0;
    }

    public int output(Node node, int level, int counter) {
        assertNotNull(node);
        NodeMetaModel metaModel = node.getMetaModel();
        if (metaModel.getType() == MethodDeclaration.class) {

            if (node instanceof NodeWithAbstractModifier) {
                if (((NodeWithAbstractModifier) node).isAbstract()) {
                    return counter;
                }
            }
            if (node.getComment().isPresent()) {
                return ++counter;
            }
        }

        List<PropertyMetaModel> allPropertyMetaModels = metaModel.getAllPropertyMetaModels();
        List<PropertyMetaModel> subLists = allPropertyMetaModels.stream().filter(PropertyMetaModel::isNodeList)
                .collect(toList());

        if (metaModel.getType() == ClassOrInterfaceDeclaration.class || metaModel.getType() == CompilationUnit.class) {
            for (PropertyMetaModel sl : subLists) {
                NodeList<? extends Node> nl = (NodeList<? extends Node>) sl.getValue(node);
                if (nl != null && nl.isNonEmpty()) {
                    for (Node nd : nl)
                        counter = output(nd, level + 1, counter);
                }
            }
        }
        return counter;
    }
*/
}
