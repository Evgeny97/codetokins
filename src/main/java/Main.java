import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;
import parcers.BaseMethodVisitor;
import parcers.StandardMethodBodyParser;
import parcers.TaggedMethodBodyParser;

import java.io.File;

public class Main {
    public static void main(String[] args) throws Exception {

        //old.DataAnalysis dataAnalysis = new old.DataAnalysis();
        //System.out.println("counter = " + dataAnalysis.CountCommentedMethods());


        //File parsedFilesFolder = new File("91g_part");
//        Preprocess p = new Preprocess();
//        System.out.println(Preprocess.addSpaces("fg fg; asd. asd.asd.asd(dasd)"));

//        System.out.println(Preprocess.remMultSpaces(Preprocess.addSpaces("+a[s]d-a<s>d*a(s)d/a{s}c=%\\t|f\"c\'o'n+=t-=a/*i*/n f*=i/=l&=e e|=xtension , and the module to be located beside the launcher . xxnl * xxnl * @ return The name of the module that is to be launched . xxnl * @ generatedxxnl * / xxnl@Override xxnl public String tagmethodname ( ) { xxnl return MODULE FILE NAME ; xxnl } \"")));

        File[] parsedFilesFolders = {new File("91g_part"),new File("11pr_dataset")};
        BaseMethodVisitor methodVisitor = new BaseMethodVisitor(false);
//        File testFiles = new File("testFiles");
        methodVisitor.visitAllClassesInFolder(parsedFilesFolders, new TaggedMethodBodyParser(), 2_000_000);


//        CompilationUnit cu = JavaParser.parse(new File(FILE_PATH));
//        YamlPrinter printer = new old.MyPrinter(true);
//        System.out.println(printer.output(cu));


    }

    private static class MethodNamePrinter extends VoidVisitorAdapter<Void> {

        @Override
        public void visit(MethodDeclaration md, Void arg) {
            super.visit(md, arg);
            if (md.getBody().isPresent()) {
                System.out.println(md.getBody().get());
            }
        }
    }
}

