package parcers;

import com.github.javaparser.ast.body.MethodDeclaration;

import static parcers.Preprocess.*;
import static parcers.Preprocess.replaceNewLineSymbols;

public class TaggedMethodBodyParser implements MethodBodyParser {
    private TaggedPrinter printer;
    @Override
    public String parseMethodBody(MethodDeclaration methodDeclaration) {
        printer = new TaggedPrinter();
        TaggedMethodBodyVisitor methodBodyVisitor = new TaggedMethodBodyVisitor(printer);
        methodBodyVisitor.visit(methodDeclaration,null);
        return doubleQuotes(printer.getSource());
    }

    @Override
    public String getFileNameExtension() {
        return ".csv";
    }
}
