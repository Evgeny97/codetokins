package parcers;

import com.github.javaparser.ast.body.MethodDeclaration;

public interface MethodBodyParser {
    String parseMethodBody(MethodDeclaration methodDeclaration);

    String getFileNameExtension();
}
