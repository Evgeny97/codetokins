package parcers;

import com.github.javaparser.JavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BaseMethodVisitor extends VoidVisitorAdapter<Void> {

    protected int methodNumber;
    private MethodBodyParser methodBodyParser;
    private FileWriter infoWriter;
    //    protected TaggedPrinter infoPrinter;
    private boolean separateFiles;

    public BaseMethodVisitor(boolean separateFiles) {
        this.separateFiles = separateFiles;
    }

    public static List<String> getAllFiles(File[] parsedFilesFolders) {
        List<String> result = new ArrayList<>();
        for (File f : parsedFilesFolders) {
            result.addAll(getAllFiles(f));
        }
        return result;
    }

    public static List<String> getAllFiles(File parsedFilesFolder) {
        List<String> result = new ArrayList<>();
        Queue<File> fileTree = new PriorityQueue<>();
        File[] files = parsedFilesFolder.listFiles();
        if (files != null) {
            Collections.addAll(fileTree, files);

            while (!fileTree.isEmpty()) {
                File currentFile = fileTree.remove();
                if (currentFile.isDirectory()) {
                    files = currentFile.listFiles();
                    if (files != null) {
                        Collections.addAll(fileTree, files);
                    }
                } else {
                    if (currentFile.getName().endsWith(".java") && !currentFile.getName().startsWith("module-info")) {
                        result.add(currentFile.getAbsolutePath());
                    }
                }
            }
            return result;
        } else {
            return new ArrayList<>(0);
        }

    }

    public void visitAllClassesInFolder(File[] parsedFilesFolders, MethodBodyParser methodBodyParser) {
        visitAllClassesInFolder(parsedFilesFolders, methodBodyParser, -1);
    }

    public void visitAllClassesInFolder(File[] parsedFilesFolders, MethodBodyParser methodBodyParser, int methodLimit) {
        List<String> filesToParse = getAllFiles(parsedFilesFolders);
        parseFiles(filesToParse, methodLimit, methodBodyParser);
    }

    public void visitAllClassesInFolder(File parsedFilesFolder, MethodBodyParser methodBodyParser) {
        visitAllClassesInFolder(parsedFilesFolder, methodBodyParser, -1);
    }

    public void visitAllClassesInFolder(File parsedFilesFolder, MethodBodyParser methodBodyParser, int methodLimit) {
        List<String> filesToParse = getAllFiles(parsedFilesFolder);
        parseFiles(filesToParse, methodLimit, methodBodyParser);
    }

    private void parseFiles(List<String> filesToParse, int methodLimit, MethodBodyParser methodBodyParser) {
        this.methodBodyParser = methodBodyParser;
        try (FileWriter infoWriter2 = new FileWriter("base\\info.csv")) {
            infoWriter = infoWriter2;

            if (separateFiles) {
                infoWriter.write("MethodName, Path\n");
            } else {
                infoWriter.write("MethodName, Body\n");
            }
            for (String filePath : filesToParse) {
                if (methodLimit > 0 && methodNumber > methodLimit) {
                    return;
                }
                System.out.println("Parsing file: " + filePath + "; counter = " + methodNumber);
                try {
                    CompilationUnit cu = JavaParser.parse(new File(filePath));
                    visit(cu, null);
                } catch (Exception e) {
                    System.err.println(e.getMessage());
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void visit(MethodDeclaration methodDeclaration, Void arg) {
        super.visit(methodDeclaration, arg);
        if (methodDeclaration.isAbstract() || methodDeclaration.isNative()) {
            return;
        }
        Pattern pattern = Pattern.compile("[A-Z][a-z]+|[A-Z]+(?![a-z])|[a-z]+|[0-9]+");
        String nameAsString = methodDeclaration.getNameAsString();
        Matcher matcher = pattern.matcher(nameAsString);
        boolean first = true;
        StringBuilder methodNameBuilder = new StringBuilder();
        while (matcher.find()) {
            String stringPart = nameAsString.substring(matcher.start(), matcher.end());
            if (!first) {
                methodNameBuilder.append(" ");
            }
            methodNameBuilder.append(stringPart);
            first = false;
        }
        String parsedMethodBody = methodBodyParser.parseMethodBody(methodDeclaration);
        methodNumber++;
        if (separateFiles) {
            try (FileWriter bodyWriter = new FileWriter("base\\dataset\\" + String.valueOf(methodNumber)
                    + methodBodyParser.getFileNameExtension())) {
                infoWriter.write(methodNameBuilder.toString() + "," + String.valueOf(methodNumber) + "\n");
                bodyWriter.write(parsedMethodBody);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                infoWriter.write(methodNameBuilder.toString() + ",\"" + parsedMethodBody + "\"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



}
