package parcers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Preprocess {
    public static String NEW_LINE_SYMBOL = "NLsNL";
    private static Pattern addSpacesPattern = Pattern.compile("("+ NEW_LINE_SYMBOL +"|//|\\+=|-=|\\*=|/=|&=|\\|=|/\\*|\\*/|[;.,\\\\+\\-*/=&?#\\\"'!%@:|$^()\\[\\]{}`<>~_])");
    private static Pattern repNewLinePattern = Pattern.compile("\r\n|\r|\n");

    public static String addSpaces(String input) {
        Matcher matcher = addSpacesPattern.matcher(input);
        return matcher.replaceAll(" $1 ");
    }

    public static String remMultSpaces(String input) {
        return input.replaceAll(" {2,}", " ");
    }

    public static String replaceNewLineSymbols(String input) {
        Matcher matcher = repNewLinePattern.matcher(input);
        return matcher.replaceAll(" " + NEW_LINE_SYMBOL + " ");
    }

    public static String doubleQuotes(String input) {
        return input.replaceAll("\"", "\"\"");
    }


}
