package parcers;

import static parcers.Preprocess.NEW_LINE_SYMBOL;

public class TaggedPrinter {
    private String path;

    private StringBuilder stringBuilder = new StringBuilder();

    public TaggedPrinter print(String tag, String arg) {
        arg = arg.trim();
        stringBuilder.append(tag).append(";").append(arg).append(" ");
        return this;
    }

    public TaggedPrinter print(String arg) {
        arg = arg.trim();
        stringBuilder.append("UNKNOWN;").append(arg).append(" ");
        return this;
    }


    public TaggedPrinter println(String tag, String arg) {
        print(tag, arg);
        println();
        return this;
    }

    public TaggedPrinter println(String arg) {
        print(arg);
        println();
        return this;
    }

    public TaggedPrinter println() {
        stringBuilder.append("NEXTLINE;TAG").append(" ");
        return this;
    }

    public String getSource() {
        return stringBuilder.toString();
    }
}
