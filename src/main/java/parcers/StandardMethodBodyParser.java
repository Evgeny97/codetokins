package parcers;

import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.printer.PrettyPrinterConfiguration;

import static parcers.Preprocess.*;

public class StandardMethodBodyParser implements MethodBodyParser {


    @Override
    public String parseMethodBody(MethodDeclaration methodDeclaration) {
        PrettyPrinterConfiguration conf = new PrettyPrinterConfiguration();
//        conf.setIndent(" ");
//        conf.setPrintComments(false);
       /* File pathFolder = new File(outputFileDirectory);
        if (!pathFolder.exists()) {
            try {
                if (!pathFolder.mkdirs()) {
//                System.err.println("Can't create directory");
                    throw new RuntimeException("Can't create directory");
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }*/
        // try (FileWriter fileWriter = new FileWriter(outputFileDirectory + outputFileName + ".txt");) {
        final StandardPrettyPrinterVisitor visitor = new StandardPrettyPrinterVisitor(conf);
        methodDeclaration.accept(visitor, null);
        //       fileWriter.write(visitor.getSource());
        //   } catch (IOException e) {
        //       e.printStackTrace();
        //    }
        return doubleQuotes(remMultSpaces(addSpaces(replaceNewLineSymbols(visitor.getSource()))));
    }

    @Override
    public String getFileNameExtension() {
        return ".txt";
    }
}
